//const connectionFactory = require('../infra/connectionFactory')
//const ProdutoDao = require('../infra/ProdutoDao.js');
module.exports = app => {
  app.get('/produtos', (req, res) => {
    const connection = app.infra.connectionFactory();
    const produtoDao = new app.infra.ProdutoDao(connection);

    produtoDao.lista((error, result, fields) => {
      res.format({
        html: function(){
          res.render('produtos/lista', {livros: result});
        },
        json: function(){
          res.json(result)
        }
      })

    });

    connection.end();
  })

  app.get('/produtos/form', function(req, res){
    res.render('produtos/form');
  })

  app.post('/produtos', (req, res) => {
    const livro = req.body;

    const connection = app.infra.connectionFactory();
    const produto = new app.infra.ProdutoDao(connection);

    produto.salva(livro,(error, result, fields) => {
      res.redirect('produtos');
    });

  });
}
