const mysql = require('mysql');

function createDBConnection() {
   return mysql.createConnection({
     user: 'root',
     password: 'caelum',
     host: 'localhost',
     database: 'casadocodigo'
   })
}

module.exports = function() {
  return createDBConnection;
}
